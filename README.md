# README #

Prueba de diagnostico para TGR

## Explicación ##

Este repositorio es para el componente Frontend, impementado en angular

la URL de git es: https://bitbucket.org/orivera/frontend/src/master/

### Local ###

ng serve, para iniciar la aplicacion 


### Nube ###

para acceder a la aplicacion, ir a la url: https://tgr-prueba-308912.uc.r.appspot.com


### Respuestas ###


Pregunta 2:
El componente que resuelve el problema es app/oauth0/login/LoginComponent, en el constructor se realiza la redirección a google

Pregunta 3:
El componente que resuelve el problema es app/oauth0/redirect/RedirectComponent, en el constructor se realiza la obtención del token y se redirige a /landing

Pregunta 4:
El componente que resuelve el problema es app/oauth0/landing/LandingComponent, donde se parsea el token y se muestra el nombre y el correo.

Pregunta 5:
En el componente app/oauth0/landing/LandingComponent, la funcion getRandomStr() implementa lo solicitado.

Pregunta 6:
Revisar el component backend.


