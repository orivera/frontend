import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomePageComponent} from './home/home-page/home-page.component';
import {FooterComponent} from './home/footer/footer.component';
import {NavbarComponent} from './home/navbar/navbar.component';
import {LoginComponent} from './oauth2/login/login.component';
import {RedirectComponent} from './oauth2/redirect/redirect.component';
import {LandingComponent} from './oauth2/landing/landing.component';
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    FooterComponent,
    NavbarComponent,
    LoginComponent,
    RedirectComponent,
    LandingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
}
