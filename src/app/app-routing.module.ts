import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from "./home/home-page/home-page.component";
import {LoginComponent} from "./oauth2/login/login.component";
import {RedirectComponent} from "./oauth2/redirect/redirect.component";
import {LandingComponent} from "./oauth2/landing/landing.component";

const routes: Routes = [
  {path: '', component: HomePageComponent, pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'redirect', component: RedirectComponent},
  {path: 'landing', component: LandingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
