import { Component, OnInit } from '@angular/core';
import {TokenModel} from "../token.model";
import jwt_decode from 'jwt-decode';
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  isValid: boolean = false;
  // @ts-ignore
  token: TokenModel;
  // @ts-ignore
  decodedToken: { [key: string]: string };
  responseText: string = '';

  constructor() {
    if (localStorage.getItem('token')){
      this.isValid = true;
      this.token = JSON.parse(localStorage.getItem('token') || '');
      this.decodedToken = jwt_decode(this.token.id_token);
    }
  }

  getRandomStr(){
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", environment.backend + "/random");
    xmlhttp.addEventListener('load', this.randomListener);
    xmlhttp.setRequestHeader('Authorization', 'Bearer ' + this.token.id_token);
    xmlhttp.send();
  }

  randomListener(){
    console.log(this.responseText);
    // @ts-ignore
    var tbody = document.getElementById('random-table').getElementsByTagName('tbody')[0];
    var newRow = tbody.insertRow();

    var newCell = newRow.insertCell();
    var newTime = document.createTextNode(Date.now().toString());
    newCell.appendChild(newTime);

    var newCell = newRow.insertCell();
    var newText = document.createTextNode(this.responseText);
    newCell.appendChild(newText);

  }

  getDecodeToken() {
    return jwt_decode(this.token.id_token);
  }

  getName() {
    return this.decodedToken ? this.decodedToken.name : null;
  }

  getEmailId() {
    return this.decodedToken ? this.decodedToken.email : null;
  }

  ngOnInit(): void {
  }

}
