import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AUTH_CLIENT_ID, AUTH_URL} from "../../config";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private httpClient: HttpClient) {
    window.location.href=AUTH_URL + 'redirect_uri=' + environment.server + '/redirect&client_id=' + AUTH_CLIENT_ID;
  }

  ngOnInit(): void {
  }

}
