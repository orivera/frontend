import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {AUTH_CLIENT_ID, AUTH_SECRET, TOKEN_URL} from "../../config";
import {TokenModel} from "../token.model";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss']
})
export class RedirectComponent implements OnInit {
  isValid: boolean = false;
  code: string = '';
  token_type: string = '';
  id_token: string = '';

  constructor(private activatedRoute: ActivatedRoute,
              private httpClient: HttpClient) {
    //obtengo los parametros desde el request

    this.activatedRoute.queryParams.subscribe(params => {
      this.code = params['code'];
      if (params['code']){
        this.isValid = true;
      }
      this.token_type = params['token_type'];
      this.id_token = params['ìd_token'];
      console.log('id_token = ' + this.id_token);
      console.log('state = ' + params['state']);
      console.log('code = ' + this.code);
      console.log('scope = ' + params['scope']);
      console.log('error = ' + params['error']);

    });


    if (this.isValid) {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded');

      const body = new HttpParams()
        .set('code', this.code)
        .set('client_id', AUTH_CLIENT_ID)
        .set('client_secret', AUTH_SECRET)
        .set('redirect_uri', environment.server + '/redirect')
        .set('grant_type', 'authorization_code')


      this.httpClient.post<TokenModel>(TOKEN_URL, body.toString(), {headers}).subscribe(
        (token: TokenModel) => {
          localStorage.setItem('token', JSON.stringify(token));
          console.log(token);
          console.log(token.id_token);
          window.location.href = environment.server + '/landing';
        }
      );
    }
  }

  ngOnInit(): void {

  }

}
