import {Component, OnInit} from '@angular/core';
import {TokenModel} from "../../oauth2/token.model";
import jwt_decode from "jwt-decode";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {REVOKE_URL} from "../../config";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  haveToken: boolean = false;
  // @ts-ignore
  token: TokenModel;
  // @ts-ignore
  decodedToken: { [key: string]: string };

  constructor(private httpClient: HttpClient) {

    if (localStorage.getItem('token')){
      this.token = JSON.parse(localStorage.getItem('token') || '');
      this.haveToken = true;
      this.decodedToken = jwt_decode(this.token.id_token);

    }

  }

  ngOnInit(): void {
  }

  getName() {
    if (this.haveToken) {
      return this.decodedToken ? this.decodedToken.name : null;
    }
    return null;
  }

  getPicture() {
    if (this.haveToken) {
      return this.decodedToken ? this.decodedToken.picture : null;
    }
    return null;
  }

  salir() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const body = new HttpParams()
      .set('token', this.token.access_token);

    this.httpClient.post(REVOKE_URL, body, {headers}).subscribe(
      () => {
        console.log('sesión finalizada');
        localStorage.removeItem('token');
        window.location.href = environment.server;
      }
    );

  }

}
